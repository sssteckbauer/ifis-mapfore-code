FCHMU60  DFHMSD TYPE=MAP,LANG=COBOL,TERM=3270,MODE=INOUT,              X
               TIOAPFX=YES,MAPATTS=(COLOR,HILIGHT),                    X
               DSATTS=(COLOR,HILIGHT)
FCHMU60  DFHMDI SIZE=(24,80),LINE=1,COLUMN=1,CTRL=FREEKB,TIOAPFX=YES
         DFHMDF POS=(01,01),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='FCHMU60',COLOR=BLUE,HILIGHT=OFF
* 'TRMNL-ID-SY00'
ZFLD002  DFHMDF POS=(01,10),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,19),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FULL-NAME-6001-SY00'
ZFLD003  DFHMDF POS=(01,23),LENGTH=0035,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,59),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'AGR-DATE'
ZFLD004  DFHMDF POS=(01,64),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'WORK-HH-MM-TIME-SY00'
ZFLD005  DFHMDF POS=(01,73),LENGTH=0005,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AM-PM-FLAG-SY00'
ZFLD006  DFHMDF POS=(01,79),LENGTH=0001,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AGR-USER-ID'
ZFLD007  DFHMDF POS=(02,01),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(02,10),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CURR-RSPNS-ID-SY00'
ZFLD008  DFHMDF POS=(02,21),LENGTH=0008,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,30),LENGTH=0001,ATTRB=(PROT,ASKIP,BRT),        X
               INITIAL=':',COLOR=DEFAULT,HILIGHT=OFF
* 'AGR-FUNC-DESCRIPTION'
ZFLD009  DFHMDF POS=(02,32),LENGTH=0028,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,61),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PAGE-STATUS-CODE-SY00'
ZFLD010  DFHMDF POS=(02,64),LENGTH=0010,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,75),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCSD-TEXT-SY01'
ZFLD011  DFHMDF POS=(03,52),LENGTH=0008,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(03,61),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CNFDL-TEXT-SY01'
ZFLD012  DFHMDF POS=(03,62),LENGTH=0011,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(03,74),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'HOLDS-TEXT-SY01'
ZFLD013  DFHMDF POS=(03,75),LENGTH=0005,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(04,01),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(04,18),LENGTH=0004,ATTRB=(PROT,ASKIP),            X
               INITIAL='COA:',COLOR=BLUE,HILIGHT=OFF
* 'COA-CODE-4030-CH60'
ZFLD014  DFHMDF POS=(04,23),LENGTH=0001,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(04,25),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(05,04),LENGTH=0018,ATTRB=(PROT,ASKIP),            X
               INITIAL='COST SHARE METHOD:',COLOR=BLUE,HILIGHT=OFF
* 'COST-SHARE-CODE-4030-CH60'
ZFLD015  DFHMDF POS=(05,23),LENGTH=0006,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(05,30),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-DESC-4064-CH60'
ZFLD016  DFHMDF POS=(05,32),LENGTH=0035,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(05,68),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,16),LENGTH=0006,ATTRB=(PROT,ASKIP),            X
               INITIAL='BASIS:',COLOR=BLUE,HILIGHT=OFF
* 'COST-SHARE-BASIS-4064-CH60'
ZFLD017  DFHMDF POS=(07,23),LENGTH=0001,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(07,25),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,29),LENGTH=0005,ATTRB=(PROT,ASKIP),            X
               INITIAL='MEMO:',COLOR=BLUE,HILIGHT=OFF
* 'COST-SHARE-MEMO-IND-4064-CH60'
ZFLD018  DFHMDF POS=(07,35),LENGTH=0001,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(07,37),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,41),LENGTH=0006,ATTRB=(PROT,ASKIP),            X
               INITIAL='APPLY:',COLOR=BLUE,HILIGHT=OFF
* 'APPLY-TO-BASIS-IND-4064-CH60'
ZFLD019  DFHMDF POS=(07,48),LENGTH=0001,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(07,50),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,54),LENGTH=0011,ATTRB=(PROT,ASKIP),            X
               INITIAL='PERCENTAGE:',COLOR=BLUE,HILIGHT=OFF
* 'COST-SHARE-STNDD-PCT-4064-CH60'
ZFLD020  DFHMDF POS=(07,66),LENGTH=0007,ATTRB=(PROT),COLOR=TURQUOISE,  X
               HILIGHT=OFF
         DFHMDF POS=(07,74),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(09,18),LENGTH=0044,ATTRB=(PROT,ASKIP),            X
               INITIAL='ACT       APPLY FROM    APPLY TO    OVERRIDE', X
               COLOR=BLUE,HILIGHT=OFF
         DFHMDF POS=(10,17),LENGTH=0044,ATTRB=(PROT,ASKIP),            X
               INITIAL='(ACDR)      ACCOUNT      ACCOUNT     PERCENT', X
               COLOR=BLUE,HILIGHT=OFF
* 'ACTN-CODE-CH60'
ZFLD021  DFHMDF POS=(11,19),LENGTH=0001,ATTRB=(UNPROT,IC),             X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(11,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD022  DFHMDF POS=(11,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(11,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD023  DFHMDF POS=(11,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(11,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD024  DFHMDF POS=(11,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(11,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD025  DFHMDF POS=(12,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(12,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD026  DFHMDF POS=(12,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(12,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD027  DFHMDF POS=(12,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(12,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD028  DFHMDF POS=(12,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(12,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD029  DFHMDF POS=(13,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(13,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD030  DFHMDF POS=(13,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(13,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD031  DFHMDF POS=(13,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(13,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD032  DFHMDF POS=(13,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(13,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD033  DFHMDF POS=(14,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(14,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD034  DFHMDF POS=(14,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(14,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD035  DFHMDF POS=(14,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(14,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD036  DFHMDF POS=(14,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(14,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD037  DFHMDF POS=(15,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(15,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD038  DFHMDF POS=(15,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(15,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD039  DFHMDF POS=(15,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(15,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD040  DFHMDF POS=(15,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(15,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD041  DFHMDF POS=(16,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(16,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD042  DFHMDF POS=(16,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(16,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD043  DFHMDF POS=(16,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(16,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD044  DFHMDF POS=(16,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(16,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD045  DFHMDF POS=(17,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(17,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD046  DFHMDF POS=(17,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(17,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD047  DFHMDF POS=(17,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(17,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD048  DFHMDF POS=(17,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(17,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD049  DFHMDF POS=(18,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(18,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD050  DFHMDF POS=(18,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(18,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD051  DFHMDF POS=(18,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(18,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD052  DFHMDF POS=(18,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(18,62),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ACTN-CODE-CH60'
ZFLD053  DFHMDF POS=(19,19),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=OFF
         DFHMDF POS=(19,21),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-FROM-ACCT-4032-CH60'
ZFLD054  DFHMDF POS=(19,30),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(19,37),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-TO-ACCT-4032-CH60'
ZFLD055  DFHMDF POS=(19,43),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(19,50),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'COST-SHARE-PCT-4032-CH60'
ZFLD056  DFHMDF POS=(19,54),LENGTH=0007,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(19,62),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(21,07),LENGTH=0015,ATTRB=(PROT,ASKIP),            X
               INITIAL='COMPLETE (Y/N):',COLOR=BLUE,HILIGHT=OFF
* 'CMPLT-IND-4064-CH60'
ZFLD057  DFHMDF POS=(21,23),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(21,25),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCLINK-MESSAGE'
ZFLD058  DFHMDF POS=(21,80),LENGTH=0160,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(24,01),LENGTH=0012,ATTRB=(PROT,ASKIP,NUM),        X
               INITIAL='NEXT SCREEN:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-PASSED-ONE'
ZFLD059  DFHMDF POS=(24,14),LENGTH=0030,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,45),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='RESPONSE:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-MAP-RESPONSE'
ZFLD060  DFHMDF POS=(24,55),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,64),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ERROR-TEXT-SY00'
ZFLD061  DFHMDF POS=(24,65),LENGTH=0014,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMSD TYPE=FINAL
         END
