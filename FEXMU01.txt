FEXMU01  DFHMSD TYPE=MAP,LANG=COBOL,TERM=3270,MODE=INOUT,              X
               TIOAPFX=YES,MAPATTS=(COLOR,HILIGHT),                    X
               DSATTS=(COLOR,HILIGHT)
FEXMU01  DFHMDI SIZE=(24,80),LINE=1,COLUMN=1,CTRL=FREEKB,TIOAPFX=YES
         DFHMDF POS=(01,01),LENGTH=0007,ATTRB=(PROT,ASKIP),            X
               INITIAL='FEXMU01',COLOR=BLUE,HILIGHT=OFF
* 'TRMNL-ID-SY00'
ZFLD002  DFHMDF POS=(01,10),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,19),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FULL-NAME-6001-SY00'
ZFLD003  DFHMDF POS=(01,23),LENGTH=0035,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(01,59),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'AGR-DATE'
ZFLD004  DFHMDF POS=(01,64),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'WORK-HH-MM-TIME-SY00'
ZFLD005  DFHMDF POS=(01,73),LENGTH=0005,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AM-PM-FLAG-SY00'
ZFLD006  DFHMDF POS=(01,79),LENGTH=0001,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
* 'AGR-USER-ID'
ZFLD007  DFHMDF POS=(02,01),LENGTH=0008,ATTRB=(PROT),COLOR=BLUE,       X
               HILIGHT=OFF
         DFHMDF POS=(02,10),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'CURR-RSPNS-ID-SY00'
ZFLD008  DFHMDF POS=(02,21),LENGTH=0008,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,30),LENGTH=0001,ATTRB=(PROT,ASKIP,BRT),        X
               INITIAL=':',COLOR=DEFAULT,HILIGHT=OFF
* 'AGR-FUNC-DESCRIPTION'
ZFLD009  DFHMDF POS=(02,32),LENGTH=0028,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,61),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'PAGE-STATUS-CODE-SY00'
ZFLD010  DFHMDF POS=(02,64),LENGTH=0010,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(02,75),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(04,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='UNIV FICE CODE         :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'FICE-CODE-EX01'
ZFLD011  DFHMDF POS=(04,26),LENGTH=0006,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(04,33),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'FULL-NAME-6001-SY00'
ZFLD012  DFHMDF POS=(04,38),LENGTH=0035,ATTRB=(PROT,ASKIP),            X
               COLOR=TURQUOISE,HILIGHT=OFF
         DFHMDF POS=(04,74),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(06,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='VENDOR CODE            :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'VNDR-ID-LAST-NINE-EX01'
ZFLD013  DFHMDF POS=(06,26),LENGTH=0009,ATTRB=(UNPROT,IC),             X
               COLOR=DEFAULT,HILIGHT=UNDERLINE
         DFHMDF POS=(06,36),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(07,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='COMMODITY CODE         :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'CMDTY-CODE-EX01'
ZFLD014  DFHMDF POS=(07,26),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(07,35),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(08,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='ADDRESS TYPE CODE      :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'ADR-TYPE-CODE-EX01'
ZFLD015  DFHMDF POS=(08,26),LENGTH=0002,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(08,29),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(09,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='COA CODE               :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'COA-CODE-EX01'
ZFLD016  DFHMDF POS=(09,26),LENGTH=0001,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(09,28),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(10,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='RELEASE NUMBER         :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'RELEASE-NMBR-EX01'
ZFLD017  DFHMDF POS=(10,26),LENGTH=0014,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(10,41),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(11,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='FILE NUMBER            :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'FILE-NMBR-EX01'
ZFLD018  DFHMDF POS=(11,26),LENGTH=0005,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(11,32),LENGTH=0,ATTRB=(ASKIP,NORM)
         DFHMDF POS=(12,01),LENGTH=0024,ATTRB=(PROT,ASKIP),            X
               INITIAL='LINE NUMBER            :',COLOR=BLUE,          X
               HILIGHT=OFF
* 'LINE-NMBR-EX01'
ZFLD019  DFHMDF POS=(12,26),LENGTH=0004,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(12,31),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'DCLINK-MESSAGE'
ZFLD020  DFHMDF POS=(21,80),LENGTH=0160,ATTRB=(PROT,BRT),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMDF POS=(24,01),LENGTH=0012,ATTRB=(PROT,ASKIP,NUM),        X
               INITIAL='NEXT SCREEN:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-PASSED-ONE'
ZFLD021  DFHMDF POS=(24,14),LENGTH=0030,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,45),LENGTH=0009,ATTRB=(PROT,ASKIP),            X
               INITIAL='RESPONSE:',COLOR=BLUE,HILIGHT=OFF
* 'AGR-MAP-RESPONSE'
ZFLD022  DFHMDF POS=(24,55),LENGTH=0008,ATTRB=(UNPROT),COLOR=DEFAULT,  X
               HILIGHT=UNDERLINE
         DFHMDF POS=(24,64),LENGTH=0,ATTRB=(ASKIP,NORM)
* 'ERROR-TEXT-SY00'
ZFLD023  DFHMDF POS=(24,65),LENGTH=0014,ATTRB=(PROT,DRK),              X
               COLOR=DEFAULT,HILIGHT=OFF
         DFHMSD TYPE=FINAL
         END
